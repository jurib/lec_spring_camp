# spring camp 수업자료


- beans.xml 


	<bean id="dataSource" class="org.apache.commons.dbcp2.BasicDataSource"
		p:driverClassName="org.h2.Driver" p:url="jdbc:h2:tcp://localhost/~/myapp" p:username="user01" p:password="1234"
	/>
	
	<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean" p:dataSource-ref="dataSource" 
		p:mapperLocations="classpath:mapper/**/*.xml"
		p:typeAliasesPackage="com.camp.model.dto"
	/>

	<mybatis-spring:scan base-package="com.camp.model.dao"/>

