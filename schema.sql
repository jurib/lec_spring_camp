drop table userinfo if exists;

create table userinfo
(
	userid varchar(12) primary key,
	password varchar(20) not null,
	name varchar(30) not null,
	email varchar(40)
);


insert into userinfo values('java','java','김태희','java@myapp.com');
insert into userinfo values('spring','spring','봄이','spring@myapp.com');